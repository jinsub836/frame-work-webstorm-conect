import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER_CREATE]: (store, payload) => {
        console.log(payload)
        return axios.post(apiUrls.DO_MEMBER_CREATE, payload)
    }
}
